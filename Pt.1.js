// я пробував різні варіанти сьогодні передивився і побачив що не той що хотів закинув, після дедлайна певно не рахується :(, але всеж докину

let result;
x = 30;
y = 10;
if (x < y) {
    result = x + y;
}
if (x > y) {
    result = x - y;
}
if (x === y) {
    result = x * y;
}
console.log(result)


// *****1*****

function sum(x, y) {
    if (x < y) {
        return x + y;
    }
    if (x > y) {
        return x - y;
    }
    if (y === x) {
        return x * y;
    }
}
console.log(sum(20, 10));


// *****2*****

let a = [1, 2, 3, -5, 0, 10];
let b = [5, -1, 7];
function arrayPlusArray(a, b) {
    let newArr = a.concat(b);
    let val = newArr.reduce(function(accumulator, currentValue){
        return accumulator + currentValue;
    });
    return val;
}
console.log(arrayPlusArray(a, b))


// *****3*****

let array = [true, false, false, true, false];
const count = array.filter(Boolean).length;
console.log(count);


// *****4*****

function doubleFactorial(n) {
    if (n === 0 || n === 1)
        return 1;
    return n * doubleFactorial(n - 2);
}
console.log(doubleFactorial(14));


// *****5*****

let person1 = {
    name: "John",
    age: 30
};
let person2 = {
    name: "Snow",
    age: 25
};
function compareAge(person1, person2) {
    if (person1.age > person2.age) {
        return `${person1.name} is older as ${person2.name}`
    }
    if (person1.age < person2.age) {
        return `${person1.name} is younger as ${person2.name}`
    }
    if (person1.age === person2.age) {
        return `${person1.name} is the same age as ${person2.name}`
    }
}
console.log(compareAge(person1, person2));


// *****5.1*****

const bigBang = [
    {name: 'Leonard', age: 24},
    {name: 'Sheldon', age: 26},
    {name: 'Penny', age: 25},
    {name: 'Howard', age: 27},
    {name: 'Rajesh', age: 22}
]
bigBang.sort(mySort)

function mySort(a, b) {
    if(a.age > b.age)
        return 1
    if(a.age < b.age)
        return -1
    return 0;
}
console.log(bigBang);


// *****5.2*****

const bestFriends = [
    {name: 'Leonard', age: 24},
    {name: 'Sheldon', age: 26},
    {name: 'Penny', age: 25},
    {name: 'Howard', age: 27},
    {name: 'Rajesh', age: 22}
]
function sortByAge(bestFriends) {
    bestFriends.sort((a, b) => a.age < b.age ? 1 : -1);
}
sortByAge(bestFriends);

console.log(bestFriends);
